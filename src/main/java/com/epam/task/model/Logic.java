package com.epam.task.model;

import java.util.List;

public class Logic implements Model {

    private StringUtils stringUtils;

    public Logic() {
        stringUtils = new StringUtils();
    }

    @Override
    public String getConcatenatesList() {
        return stringUtils.getConcatenatesList();
    }

    @Override
    public boolean checkEnteredWord() {
        return stringUtils.checkEnteredWord();
    }

    @Override
    public String splitEnteredSentence() {
        return stringUtils.splitEnteredSentence();
    }

    @Override
    public String replaceVowel() {
        return stringUtils.replaceVowel();
    }
}
