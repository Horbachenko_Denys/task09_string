package com.epam.bigTask.comparator;

import com.epam.bigTask.model.StringUtils;

import java.util.Comparator;

public class VowelPercentageComparator implements Comparator <String> {
    @Override
    public int compare(String s1, String s2) {
        int s1VowelPercentage = StringUtils.countVowelPercentage(s1);
        int s2VowelPercentage = StringUtils.countVowelPercentage(s2);
        if (s1VowelPercentage > s2VowelPercentage) {
            return 1;
        } else if (s1VowelPercentage == s2VowelPercentage) {
            return 0;
        }
        return -1;
    }
}
