package com.epam.bigTask.comparator;

import java.util.Comparator;

public class FirstVowelLetterComparator implements Comparator<String> {
    @Override
    public int compare(String s1, String s2) {
        s1 = s1.replaceAll("[aeouiAEOIU]", "");
        s2 = s2.replaceAll("[aeouiAEOIU]", "");
        return s1.compareTo(s2);
    }
}
