package com.epam.bigTask.comparator;

import java.util.Comparator;

public class SentenceComparator implements Comparator<String> {
    @Override
    public int compare(String s1, String s2) {
        String[] word1 = s1.split(" ");
        String[] word2 = s2.split(" ");
        if (word1.length > word2.length) {
            return 1;
        } else if (word1.length == word2.length) {
            return 0;
        }
        return -1;
    }
}
