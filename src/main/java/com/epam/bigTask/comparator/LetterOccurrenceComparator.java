package com.epam.bigTask.comparator;

import java.util.Comparator;

public class LetterOccurrenceComparator implements Comparator<String> {
    char letter;

    public LetterOccurrenceComparator(char letter) {
        this.letter = letter;
    }

    @Override
    public int compare(String s1, String s2) {
        long s1LetterCount = s1
                .chars()
                .filter(ch -> ch == letter)
                .count();

        long s2LetterCount = s2
                .chars()
                .filter(ch -> ch == letter)
                .count();
        if (s1LetterCount < s2LetterCount) {
            return 1;
        } else if (s1LetterCount == s2LetterCount) {
            return 0;
        }
        return -1;
    }
}
