package com.epam.bigTask.comparator;

import java.util.Comparator;

public class LetterOccurrenceByAlphabetComparator implements Comparator<String> {
    char letter;

    public LetterOccurrenceByAlphabetComparator(char letter) {
        this.letter = letter;
    }

    @Override
    public int compare(String s1, String s2) {
        long s1LetterCount = s1
                .chars()
                .filter(c -> c == letter)
                .count();
        long s2LetterCount = s2
                .chars()
                .filter(c -> c == letter)
                .count();
        if (s1LetterCount > s2LetterCount) {
            return -1;
        } else  if (s1LetterCount == s2LetterCount){
            return 0;
        }
        return 1;
    }
}
