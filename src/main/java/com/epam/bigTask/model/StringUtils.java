package com.epam.bigTask.model;


import com.epam.bigTask.comparator.*;

import java.util.*;
import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class StringUtils {

    public static List<String> getSentenceIncreasingOrder(List<String> text) {
        text.sort(new SentenceComparator());
        return text;
    }

    private static <K, V extends  Comparable<? super  V>> Map<K, V> sortMapByValue(Map<K, V> map) {
        List<Map.Entry<K, V>> list = new ArrayList<>(map.entrySet());
        list.sort(Map.Entry.comparingByValue());
        Collections.reverse(list);
        Map<K, V> result = new LinkedHashMap<>();
        for (Map.Entry<K, V> entry : list) {
            result.put(entry.getKey(), entry.getValue());
        }
        return result;
    }

    public static int countVowelPercentage (String word) {
        int vowelCounter = 0;
        int vowelPercentage= 0;
        for (char c : word.toCharArray()) {
            switch (c) {
                case 'a':
                case 'e':
                case 'u':
                case 'i':
                case 'o':
                    vowelCounter++;
                    break;
            }
            if (vowelCounter != 0) {
                vowelPercentage = word.length() / vowelCounter;
            }
        }
        return vowelPercentage;
    }

    public static  String findUniqueWord (List<String> text) {
        String sentence = text.get(0);
        String[] sentenceWords = sentence.split(" ");
        String uiqueWord = "";
        for (String s : text) {
            String[] words = sentence.split("");
            for (String sentenceWord : sentenceWords) {
                for (String word : words) {
                    if (!word.equals(sentenceWord)) {
                        uiqueWord = sentenceWord;
                    }
                }
            }
        }
        return uiqueWord;
    }

    public static  List<String> findQuestionSentence(List<String> text, int length) {
        List<String> questions = text
                .stream()
                .filter(t -> t.endsWith("?"))
                .collect(Collectors.toList());
        List<String> uniqueQuestionsWords = questions
                .stream()
                .distinct()
                .collect(Collectors.toList());
        return uniqueQuestionsWords
                .stream()
                .filter(q -> q.length() == length)
                .collect(Collectors.toList());
    }

    public  static List<String> changeWordWithLongest(List<String> text){
        for (String sentence : text) {
            String firstWordStartsVowel = "";
            String[] words = sentence.split(" ");
            for (String w : words) {
                if (w.startsWith("[aeouiAEUIO]")) {
                    firstWordStartsVowel = w;
                }
            }
            String longestWord = Arrays.stream(words)
                    .max(Comparator.comparingInt(String::length))
                    .get();
            int firstWordsStartVowelIndex = text.indexOf(firstWordStartsVowel);
            int longestWordIndex = text.indexOf(longestWord);

            if (firstWordsStartVowelIndex > 0 && longestWordIndex > 0) {
                text.add(firstWordsStartVowelIndex, longestWord);
                text.add(longestWordIndex, firstWordStartsVowel);
            }
        }
        return text;
    }

    public static List<String> printWordsInAlphabeticalOrder(List<String> text) {
        List<String> sortedText = new ArrayList<>();
        for (String sentence : text) {
            List<String> words = Arrays.asList(sentence.split(" "));
            Collections.sort(words);
            sortedText.addAll(words);
        }
        Collections.sort(sortedText);
        return sortedText;
    }

    public static  List<String> sortPercentageVowel(List<String> text) {
        List<String> sorted = new ArrayList<>();
        for (String sentence : text) {
            List<String> words = Arrays.asList(sentence.split(" "));
            words.sort(new VowelPercentageComparator());
            sorted.addAll(words);
        }
        sorted.sort(new VowelPercentageComparator());
        return sorted;
    }

    public static List<String> sortWordsStartsWithVowel(List<String> text) {
        List<String> wordsStartsWithVowel = new ArrayList<>();
        Pattern pattern = Pattern.compile("^[aeoiu|AEOUI]");
        Matcher matcher;
        for (String sentence : text) {
            String[] words = sentence.split(" ");
            for (String w : words) {
                matcher = pattern.matcher(w);
                if (matcher.lookingAt()) {
                    wordsStartsWithVowel.add(w);
                }
            }
        }
        wordsStartsWithVowel.sort(new FirstVowelLetterComparator());
        return  wordsStartsWithVowel;
    }

    public static List<String> sortWordsByLetter(List<String> text, char letter) {
        List<String> sorted = new ArrayList<>();
        for (String sentence : text) {
            List<String> words = Arrays.asList(sentence.split(" "));
            words.sort(new LetterOccurrenceComparator(letter));
            sorted.addAll(words);
        }
        sorted.sort(new LetterOccurrenceComparator(letter));
        return sorted;
    }

    public static Set<String> findOccurOfLetter(List<String> text, List<String> wordsToCount) {
        Map<String, Long> wordCount = new HashMap<>();
        for (String sentence : text) {
            List<String> words = Arrays.asList(sentence.split(" "));
            for (String wordToCount : wordsToCount) {
                long count = words.stream().filter(w -> w.equalsIgnoreCase(wordToCount)).count();
                wordCount.put(wordToCount, count);
            }
        }
        wordCount = sortMapByValue(wordCount);
        return wordCount.keySet();
    }

    public static List<String> deleteWordsStartConsonant(List<String> text, int length) {
        List<String> result = new ArrayList<>();
        Predicate<String> predicate = Pattern.compile("^[^aeuioAEIOU]").asPredicate();
        for (String sentence : text) {
        List<String> words = Arrays.asList(sentence.split(" "));
        List<String> filtered = words
                .stream()
                .filter(predicate)
                .filter(w -> w.length() != length)
                .collect(Collectors.toList());
        result.addAll(filtered);
        }
        return result;
    }

    public static List<String> sortDecreasingByLetter(List<String> text, char letter) {
        List<String> result = sortWordsByLetter(text, letter);
        result.sort(new LetterOccurrenceByAlphabetComparator(letter));
        return result;
    }

    public static  List<String> changeWordsBySubstring(List<String> text, int length, String substring) {
        List<String> result = new ArrayList<>();
        for (String sentence : text) {
            String[] words = sentence.split(" ");
            for (String word : words) {
                if (word.length() == length && word.length() != substring.length()) {
                    word = word.replace(substring, "");
                    result.add(word);
                }
            }
        }
        return result;
    }
}