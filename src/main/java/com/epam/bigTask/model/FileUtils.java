package com.epam.bigTask.model;

import java.io.IOException;
import java.util.List;

public interface FileUtils {
    List<String> readFromFile (String path) throws IOException;
}
