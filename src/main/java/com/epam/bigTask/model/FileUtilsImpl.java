package com.epam.bigTask.model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FileUtilsImpl implements FileUtils{

    @Override
    public List<String> readFromFile(String filePath) throws IOException {
        File file = new File(filePath);
        BufferedReader bf = new BufferedReader(new FileReader(file));
        List<String> lines = new ArrayList<>();
        for (String line;(line = bf.readLine()) !=null;) {
            lines.add(line);
        }
        List<String> result = new ArrayList<>();
        for (String line : lines) {
            String [] sentences = line.split("[.!?]");
            result.addAll(Arrays.asList(sentences));
        }
        return result;
    }
}
