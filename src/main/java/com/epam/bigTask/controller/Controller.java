package com.epam.bigTask.controller;

public interface Controller {

    void getLargestNumber();

    void getSentenceIncreasingOrder();

    void findUniqueWord();

    void findQuestionSentence();

    void changeWordWithLongest();

    void printWordsInAlphabeticalOrder();

    void sortPercentageVowel();

    void sortWordsStartsWithVowel();

    void sortWordsByLetter();

    void findOccurOfLetter();

    void deleteSubstringBoundedBy();

    void deleteWordsStartConsonant();

    void sortDecreasingByLetter();

    void findLongestPalindrome();

    void convertWords();

    void changeWordsBySubstring();

}
