package com.epam.bigTask.controller;


import com.epam.bigTask.model.FileUtils;
import com.epam.bigTask.model.StringUtils;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class ControllerImpl implements Controller {
    private FileUtils fileUtils;
    private String filePath = "E:\\Денис\\Навчання\\IT\\Java\\EPAM\\HomeWorks\\task09_string\\src\\main\\resources\\text1.txt";

    public ControllerImpl(FileUtils fileUtils) {
        this.fileUtils = fileUtils;
    }

    @Override
    public void getLargestNumber() {

    }

    @Override
    public void getSentenceIncreasingOrder() {
        try {
            List<String> orderedText = StringUtils.getSentenceIncreasingOrder(fileUtils.readFromFile(filePath));
            orderedText.forEach(s -> System.out.print(s + " "));
            System.out.println();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void findUniqueWord() {
        try {
            List<String> text = fileUtils.readFromFile(filePath);
            System.out.println(StringUtils.findUniqueWord(text));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void findQuestionSentence() {
        Scanner scanner = new Scanner(System.in);
        int length = scanner.nextInt();
        try {
            StringUtils.findQuestionSentence(fileUtils.readFromFile(filePath), length)
                    .forEach(w -> System.out.print(w + " "));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void changeWordWithLongest() {
        try {
            StringUtils.changeWordWithLongest(fileUtils.readFromFile(filePath))
                    .forEach(s -> System.out.print(s + " "));
            System.out.println();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void printWordsInAlphabeticalOrder() {
        try {
            StringUtils.printWordsInAlphabeticalOrder(fileUtils.readFromFile(filePath))
                    .forEach(System.out::println);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void sortPercentageVowel() {
        try {
            StringUtils.sortPercentageVowel(fileUtils.readFromFile(filePath))
                    .forEach(System.out::println);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void sortWordsStartsWithVowel() {
        try {
            StringUtils.sortWordsStartsWithVowel(fileUtils.readFromFile(filePath))
                    .forEach(System.out::println);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void sortWordsByLetter() {
        Scanner scanner = new Scanner(System.in);
        char letter = scanner.next().charAt(0);
        try {
            StringUtils.sortWordsByLetter(fileUtils.readFromFile(filePath), letter)
                    .forEach(System.out::println);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void findOccurOfLetter() {
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        List<String> words = Arrays.asList(input.split(" "));
        try {
            StringUtils.findOccurOfLetter(fileUtils.readFromFile(filePath), words)
                    .forEach(System.out::println);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteSubstringBoundedBy() {

    }

    @Override
    public void deleteWordsStartConsonant() {
        Scanner scanner = new Scanner(System.in);
        int length = scanner.nextInt();
        try {
            StringUtils.deleteWordsStartConsonant(fileUtils.readFromFile(filePath), length)
                    .forEach(System.out::println);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void sortDecreasingByLetter() {
        Scanner scanner = new Scanner(System.in);
        char letter = scanner.nextLine().charAt(0);
        try {
            StringUtils.sortDecreasingByLetter(fileUtils.readFromFile(filePath), letter)
                    .forEach(System.out::println);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void findLongestPalindrome() {

    }

    @Override
    public void convertWords() {

    }

    @Override
    public void changeWordsBySubstring() {
        Scanner scanner = new Scanner(System.in);
        int wordLength = scanner.nextInt();
        scanner.nextLine();
        String subString = scanner.nextLine();
        try {
            StringUtils.changeWordsBySubstring(fileUtils.readFromFile(filePath), wordLength, subString)
                    .forEach(System.out::println);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}