package com.epam.bigTask.view;

import com.epam.bigTask.controller.Controller;
import com.epam.bigTask.controller.ControllerImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class MyView {

    public static Logger logger = LogManager.getLogger(MyView.class);
    private static Scanner input = new Scanner(System.in);
    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;

    public MyView(Controller controller) {
        this.controller = controller;
        setMenu();
        this.methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);
        methodsMenu.put("5", this::pressButton5);
        methodsMenu.put("6", this::pressButton6);
        methodsMenu.put("7", this::pressButton7);
        methodsMenu.put("8", this::pressButton8);
        methodsMenu.put("9", this::pressButton9);
        methodsMenu.put("10", this::pressButton10);
        methodsMenu.put("11", this::pressButton11);
        methodsMenu.put("12", this::pressButton12);
        methodsMenu.put("13", this::pressButton13);
        methodsMenu.put("14", this::pressButton14);
        methodsMenu.put("15", this::pressButton15);
        methodsMenu.put("16", this::pressButton16);
    }

    private void setMenu(){
        menu = new LinkedHashMap<>();
        menu.put("1", " 1 - find the largest number of sentences that have same words...");
        menu.put("2", " 2 - show all sentence in increasing order by numbers of words...");
        menu.put("3", " 3 - find word in first sentence that unique for all text...");
        menu.put("4", " 4 - find and print in all question sentence word given length...");
        menu.put("5", " 5 - in every sentence change first word with the longest...");
        menu.put("6", " 6 - print words in alphabetical order...");
        menu.put("7", " 7 - sort words by percentage of vowels...");
        menu.put("8", " 8 - words starts with vowels sort in alphabetical order...");
        menu.put("9", " 9 - sort all words by numbers of giving letter...");
        menu.put("10", " 10 - find number of occurs for all given words");
        menu.put("11", " 11 - delete substring with maximum length of bounded by given words...");
        menu.put("12", " 12 - delete all words of given length that start with consonant letter...");
        menu.put("13", " 13 - sort word in decreasing order by occur of given letter...");
        menu.put("14", " 14 - find the longest word which is palindrome...");
        menu.put("15", " 15 - convert all words in text ...");
        menu.put("16", " 16 - change words of given length by given substring...");
        menu.put("Q", " Q - Quit");
    }

    private void pressButton1() {
        controller.getLargestNumber();
    }

    private void pressButton2() {
        controller.getSentenceIncreasingOrder();
    }

    private void pressButton3() {
        controller.findUniqueWord();
    }

    private void pressButton4() {
        controller.findQuestionSentence();
    }

    private void pressButton5() {
        controller.changeWordWithLongest();

    }private void pressButton6() {
        controller.printWordsInAlphabeticalOrder();
    }

    private void pressButton7() {
        controller.sortPercentageVowel();
    }

    private void pressButton8() {
        controller.sortWordsStartsWithVowel();
    }

    private void pressButton9() {
        controller.sortWordsByLetter();
    }

    private void pressButton10() {
        controller.findOccurOfLetter();
    }

    private void pressButton11() {
        controller.deleteSubstringBoundedBy();
    }

    private void pressButton12() {
        controller.deleteWordsStartConsonant();
    }

    private void pressButton13() {
        controller.sortDecreasingByLetter();
    }

    private void pressButton14() {
        controller.findLongestPalindrome();
    }

    private void pressButton15() {
        controller.convertWords();
    }

    private void pressButton16() {
        controller.changeWordsBySubstring();
    }

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
                e.printStackTrace();
                logger.error("Error in menu");
            }
        } while (!keyMenu.equals("Q"));
    }
}
