package com.epam.bigTask;


import com.epam.bigTask.controller.Controller;
import com.epam.bigTask.controller.ControllerImpl;
import com.epam.bigTask.model.FileUtils;
import com.epam.bigTask.model.FileUtilsImpl;
import com.epam.bigTask.view.MyView;

public class Main {
    public static void main(String[] args) {
        FileUtils fileUtils = new FileUtilsImpl();
        Controller controller = new ControllerImpl(fileUtils);
        new MyView(controller).show();
    }
}
